package swtotp

import (
	"crypto/hmac"
	"crypto/rand"
	"encoding/base32"
	"encoding/binary"
	"errors"
	"fmt"
	"math"
	"net/url"
	"strconv"
	"strings"
	"time"

	qrcode "sw.totp/qrcode"
)

func New(opts TotpOpts) (*OTPAuth, error) {
	if len(opts.Account) == 0 {
		return nil, errors.New("miss account setting")
	}

	if opts.Expire < cst_EXPIRE_DEFAULT {
		opts.Expire = cst_EXPIRE_DEFAULT
	}

	if opts.CodeLen == 0 {
		opts.CodeLen = CST_CODE_LEN_6
	}

	secret := make([]byte, cst_SECRET_LEN)

	_, err := rand.Reader.Read(secret)
	if err != nil {
		return nil, err
	}

	u := url.Values{}
	u.Set(cst_K_ISSUER, cst_DES_ISSUER)
	u.Set(cst_K_ALGORITHM, opts.HMAC.String())
	u.Set(cst_K_DIGITS, fmt.Sprintf("%d", opts.CodeLen))
	u.Set(cst_K_PERIOD, strconv.Itoa(opts.Expire))
	u.Set(cst_K_SECRET, base32.StdEncoding.WithPadding(base32.NoPadding).EncodeToString(secret))

	k := url.URL{
		Scheme:   cst_K_OTPAUTH,
		Host:     cst_K_TOTP,
		Path:     "/" + cst_DES_ISSUER + ":" + opts.Account,
		RawQuery: u.Encode(),
	}

	urlKey, err := url.Parse(strings.TrimSpace(k.String()))
	if err != nil {
		return nil, err
	}

	return &OTPAuth{
		urlKey: urlKey,
	}, nil
}

func Code(secret string, expire int, cLen codeLen, hmacAlgo hmacAlgo) (string, error) {
	if expire < 30 {
		expire = 30
	}

	t := time.Now()
	cnt := uint64(math.Floor(float64(t.Unix()) / float64(expire)))

	secret = strings.TrimSpace(secret)
	if n := len(secret) % 8; n != 0 {
		secret = secret + strings.Repeat("=", 8-n)
	}

	secret = strings.ToUpper(secret)

	sctBytes, err := base32.StdEncoding.DecodeString(secret)
	if err != nil {
		return "", errors.New("fail to decode base32 secret")
	}

	buf := make([]byte, 8)
	sha := hmac.New(hmacAlgo.Hash, sctBytes)
	binary.BigEndian.PutUint64(buf, cnt)

	sha.Write(buf)
	sum := sha.Sum(nil)

	// RFC 4226
	offset := sum[len(sum)-1] & 0xf
	value := int64(((int(sum[offset]) & 0x7f) << 24) |
		((int(sum[offset+1] & 0xff)) << 16) |
		((int(sum[offset+2] & 0xff)) << 8) |
		(int(sum[offset+3]) & 0xff))

	return cLen.fmtZero(int32(value % int64(math.Pow10(int(cLen))))), nil
}

func QRCode(key string, size int) ([]byte, error) {
	var png []byte
	png, err := qrcode.Encode(key, qrcode.Medium, size)
	if err != nil {
		return make([]byte, 0), err
	}

	return png, nil
}
