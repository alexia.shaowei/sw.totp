package swtotp

import "fmt"

type codeLen int

func (ref codeLen) fmtZero(code int32) string {
	stuffing := fmt.Sprintf("%%0%dd", ref) // %06d or %08d

	return fmt.Sprintf(stuffing, code)
}
