package swtotp

const (
	CST_CODE_LEN_6 codeLen = 6
	CST_CODE_LEN_8 codeLen = 8
)

const (
	CST_HMAC_SHA1   hmacAlgo = 0
	CST_HMAC_SHA256 hmacAlgo = 1
	CST_HMAC_SHA512 hmacAlgo = 2
)

const (
	cst_DES_SHA1   = "SHA1"
	cst_DES_SHA256 = "SHA256"
	cst_DES_SHA512 = "SHA512"
)

const (
	cst_DES_ISSUER = "future.net.co.wl2"
)

const (
	cst_EXPIRE_DEFAULT = 30
	cst_SECRET_LEN     = 20
)

// https://github.com/google/google-authenticator/wiki/Key-Uri-Format
const (
	cst_K_OTPAUTH   = "otpauth"
	cst_K_TOTP      = "totp"
	cst_K_SECRET    = "secret"
	cst_K_ISSUER    = "issuer"
	cst_K_PERIOD    = "period"
	cst_K_ALGORITHM = "algorithm"
	cst_K_DIGITS    = "digits"
)
