package swtotp

type TotpOpts struct {
	Account string
	Expire  int

	CodeLen codeLen
	HMAC    hmacAlgo
}
